const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const Slimbot = require('slimbot');
const mongoose = require('mongoose');
const request = require('request-promise');

// Telegram chatbot connection with key
const slimbot = new Slimbot('311573148:AAFrVEeWFqzWxj7PM8ctaZd2NXauxG-FfAQ');

// Connect to DB
mongoose.connect('mongodb://localhost/challenge-robos-db');

const db = mongoose.connection;
const CHATBOT_USERNAME = 'challenge_robos_claudio_bot';

db.on('error', console.error.bind(console, 'connection error:'));

function handleSaveStatus(err) {
  if (err) {
    console.log(err);
  } else {
    console.log('Message registered.');
  }
}

// Chat model
const Chat = mongoose.model('Chat', {
  first_name: String,
  username: String,
  chat_id: String,
  message: String,
  date: String,
});

// Received telegram message from someone
slimbot.on('message', (message) => {
  // Create new model
  const chatmsg = new Chat({
    first_name: message.chat.first_name,
    username: message.from.username,
    chat_id: message.chat.id,
    message: message.text,
    date: +new Date(),
  });

  // Save it to DB
  chatmsg.save(handleSaveStatus);
});

// Actions happening/focusing on Websockets
io.on('connection', (socket) => {
  // Admin sent message via web application
  socket.on('client:message', (msg) => {
    // Send message directly to Telegram chat
    slimbot.sendMessage(msg.chat_id, msg.message);

    // Create new Model
    const chatmsg = new Chat({
      username: CHATBOT_USERNAME,
      chat_id: msg.chat_id.toString(),
      message: msg.message,
      date: +new Date(),
    });

    // Save it to DB
    chatmsg.save(handleSaveStatus);
  });

  // Received telegram message from someone
  slimbot.on('message', (message) => {
    // Send message to webapp
    socket.emit('server:message', {
      first_name: message.chat.first_name,
      username: message.chat.username,
      chat_id: message.chat.id,
      message: message.text,
      date: +new Date(),
    });

    // Send menu list to update menu
    Chat.aggregate([{ $group: { _id: { chat_id: '$chat_id', username: '$username', first_name: '$first_name' } } }], (err, messages) => {
      if (err) return console.error(err);

      // Filter chat windows from the chatbot itself
      const filteredMessages = messages.filter(msg => msg._id.username != CHATBOT_USERNAME);

      // Send update to webapp for menu update
      socket.emit('server:chat', filteredMessages);

      return true;
    });
  });
});

// Middleware to allow CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, FB-Login-Token');
  next();
});

// REST /chats
app.get('/chats', (req, res, next) => {
  const FB_LOGIN_TOKEN = req.headers['fb-login-token'];
  if (!FB_LOGIN_TOKEN) {
    res.json({ access: 'NOT AUTHORIZED' });
    next();
    return;
  }
  request(`https://graph.facebook.com/me?access_token=${FB_LOGIN_TOKEN}`).then(() => {
    Chat.aggregate([{ $group: { _id: { chat_id: '$chat_id', username: '$username', first_name: '$first_name' } } }], (err, messages) => {
      if (err) return console.error(err);

      // Filter chat windows from the chatbot itself
      const filteredMessages = messages.filter(msg => msg._id.username != CHATBOT_USERNAME);
      
      res.json(filteredMessages);
      next();
      return true;
    });
  })
  .catch((err) => {
    console.log(err);
    res.json({ access: 'NOT AUTHORIZED', error: err.response.body });
    next();
  });
});

// REST /chats/:id
app.get('/chats/:id', (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  const FB_LOGIN_TOKEN = req.headers['fb-login-token'];
  if (!FB_LOGIN_TOKEN) {
    res.json({ access: 'NOT AUTHORIZED' });
    next();
    return;
  }
  request(`https://graph.facebook.com/me?access_token=${FB_LOGIN_TOKEN}`).then(() => {
    Chat.find({ chat_id: req.params.id }, (err, messages) => {
      if (err) return console.error(err);
      res.json(messages);
      next();
      return true;
    });
  })
  .catch((err) => {
    res.json({ access: 'NOT AUTHORIZED', error: err.response.body });
    next();
  });
});

// Call API
slimbot.startPolling();

http.listen(3000, () => {
  console.log('listening on *:3000');
});
