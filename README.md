# ROBOS.im Challenge

**Note: This is just the back-end part of the project. You still need the front-end running in order to run the example. [You can get it here](https://bitbucket.org/kazzkiq/challenge-robos-frontend).**

**The Telegram Bot is: `@challenge_robos_claudio_bot`**

### Instructions:

Make sure you have Mongo, Node.js and NPM installed.

To run the server, first clone this repo, then:

1. Run `npm install`;
2. Run `npm start`;

The server will lift both HTTP and WS services at `http://localhost:3000`.

REST URLs are:

- GET `/chats` - return a list with all chats grouped by chat id.
- GET `/chats/{id}` - return all messages from a certain chat, based on its id.

There are no POST/PUT methods, as all messages are recorded in the database directly when received from the websocket connection.